package relations.OneToOneEx1;


import org.hibernate.Session;
import org.hibernate.SessionFactory;
import relations.OneToOneEx1.Customer;
import relations.OneToOneEx1.Address;
import util.HibernateUtil;

/**
 * Created by jan_w on 27.09.2017.
 */
public class Main {

    public static void main(String[] args) {
        SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
        Session session = sessionFactory.openSession();

        try {
            session.beginTransaction();

            Address address = new Address("Długa", "Gdańsk");
            session.persist(address);

            Customer customer = new Customer("Wiechu", address);
            session.persist(customer);

            session.getTransaction().commit();

        } catch (Throwable e) {
            session.getTransaction().rollback();
        } finally {
            session.close();
            sessionFactory.close();
        }
    }
}