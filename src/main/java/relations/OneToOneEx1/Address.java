package relations.OneToOneEx1;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * Created by jan_w on 27.09.2017.
 */

@Entity
public class Address {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String street;

    private String town;

    public Address() {
    }

    public Address(String street, String town) {
        this.street = street;
        this.town = town;
    }
}
