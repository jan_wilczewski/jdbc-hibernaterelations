package relations.OneToManyEx2;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import relations.OneToMany.User;
import util.HibernateUtil;

import java.util.List;

/**
 * Created by RENT on 2017-09-28.
 */
public class Main {

    public static void main(String[] args) {
        SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
        Session session = sessionFactory.openSession();

        try {
            session.beginTransaction();

//            Animal reksio = new Animal("Reksio", "pies");
//            Animal tom = new Animal("Tom", "kot");
//            Animal jerry = new Animal("Jerry", "mysz");
//            Animal tweety = new Animal("Tweety", "kanarek");
//
//            Owner bolek = new Owner("Bolek", 12);
//            Owner lolek = new Owner("Lolek", 15);
//
//            session.persist(bolek);
//            session.persist(lolek);
//
//            bolek.getAnimals().add(reksio);
//            lolek.getAnimals().add(tom);
//            lolek.getAnimals().add(jerry);
//
//            reksio.setOwner(bolek);
//            tom.setOwner(lolek);
//            jerry.setOwner(lolek);
//
//            session.persist(reksio);
//            session.persist(tom);
//            session.persist(jerry);
//            session.persist(tweety);

//            List<Animal> koty = (List<Animal>) session.createQuery("from Animal a where a.breed = :breed")
//                    .setParameter("breed", "kot")
//                    .getResultList();
//            System.out.println(koty);

            List<Animal> animalWithoutOwner = (List<Animal>)
                    session.createQuery("select a from Animal a left join p.owner where p.owner_id is NULL")
                    .getResultList();
            System.out.println(animalWithoutOwner);

            session.getTransaction().commit();
        }catch (Throwable e){
            System.out.println("ROLLBACK");
            e.printStackTrace();
        }
    }
}

