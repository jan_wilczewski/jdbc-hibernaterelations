package relations.OneToMany;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import util.HibernateUtil;

import java.util.List;

/**
 * Created by RENT on 2017-09-28.
 */
public class Main {

    public static void main(String[] args) {
        SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
        Session session = sessionFactory.openSession();

        try {
            session.beginTransaction();

//            User user1 = new User("mietek");
//            User user2 = new User("heniek");
//            User user3 = new User("janusz");
//            session.persist(user3);
//
//            session.persist(user1);
//            session.persist(user2);
//
//            Phone phone1 = new Phone("123456789", "nokia");
//            Phone phone2 = new Phone("9876543321", "huawei");
//            Phone phone3 = new Phone("9876543321", "lg");
//
//            session.persist(phone1);
//            session.persist(phone2);
//            session.persist(phone3);
//
//            user1.getPhones().add(phone1);
//            user1.getPhones().add(phone2);
//            user2.getPhones().add(phone3);

//            User user = session.find(User.class, 1);
//            System.out.println(user.getPhones());

//            List<User> users = (List<User>)session.createQuery("from User")
//                    .getResultList();
//            System.out.println(users);
//
//            List<Phone> phones = (List<Phone>) session.createQuery("from Phone p where p.number = :number")
//                    .setParameter("number", "9876543321")
//                    .getResultList();
//            System.out.println(phones);

//            List<User> user2 = (List<User>) session.createQuery("select u from User u join u.phones p where p.number = :number")
//                    .setParameter("number", "9876543321")
//                    .getResultList();
//            for (User u: user2){
//                System.out.println(u);
//            }

            List<User> userWithoutPhone = (List<User>) session.createQuery("select u from User u left join u.phones p where p.number is NULL")
                .getResultList();
            System.out.println(userWithoutPhone);

            session.getTransaction().commit();
        }catch (Throwable e){
            System.out.println("ROLLBACK");
            e.printStackTrace();
        }
    }
}
