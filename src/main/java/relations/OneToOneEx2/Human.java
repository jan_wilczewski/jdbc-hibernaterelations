package relations.OneToOneEx2;

import javax.persistence.*;

/**
 * Created by jan_w on 27.09.2017.
 */

@Entity
public class Human {

    @Id
    private int id;
    private String name;

    @OneToOne
    @PrimaryKeyJoinColumn
    private Brain brain;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Brain getBrain() {
        return brain;
    }

    public void setBrain(Brain brain) {
        this.brain = brain;
    }

    @Override
    public String toString() {
        return "Human{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", brain=" + brain +
                '}';
    }
}
