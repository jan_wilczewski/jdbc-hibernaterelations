package relations.OneToOneEx2;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import util.HibernateUtil;

/**
 * Created by jan_w on 27.09.2017.
 */
public class Main {

    public static void main(String[] args) {
        SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
        Session session = sessionFactory.openSession();

        try {
            session.beginTransaction();

            Brain brain = new Brain();
            brain.setName("móżdżek33");
            session.persist(brain);

            Human human = new Human();
            human.setName("Roman");
            human.setBrain(brain);
            human.setId(brain.getId());
            session.persist(human);

            Human humanH = session.find(Human.class,2);
            System.out.println("human: " + humanH);

            session.getTransaction().commit();

        } catch (Throwable e) {
            session.getTransaction().rollback();
        } finally {
            session.close();
            sessionFactory.close();
        }
    }
}
