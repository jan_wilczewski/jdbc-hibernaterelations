package relations.ManyToMany;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import relations.OneToManyEx2.Animal;
import util.HibernateUtil;

import java.util.List;

/**
 * Created by RENT on 2017-09-28.
 */
public class Main {

    public static void main(String[] args) {
        SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
        Session session = sessionFactory.openSession();

        try {
            session.beginTransaction();

//            Town Gdynia = new Town("Gdynia", "Poland");
//            Town NewYork = new Town("New York", "USA");
//            Town Barcelona = new Town("Barcelona", "Hiszpania");
//
//            session.persist(Gdynia);
//            session.persist(NewYork);
//            session.persist(Barcelona);
//
//            Attraction park = new Attraction("Central Park", "super pakr");
//            Attraction kino = new Attraction("kino", "nowe kino");
//            Attraction larambla = new Attraction("larambla", "wielki larambla");
//            Attraction molo = new Attraction("molo", "drewniane");
//
//            session.persist(park);
//            session.persist(kino);
//            session.persist(larambla);
//            session.persist(molo);
//
//            NewYork.getAttractions().add(park);
//            park.getTowns().add(NewYork);
//
//            NewYork.getAttractions().add(kino);
//            kino.getTowns().add(NewYork);
//
//            Gdynia.getAttractions().add(molo);
//            molo.getTowns().add(Gdynia);
//
//            Barcelona.getAttractions().add(larambla);
//            larambla.getTowns().add(Barcelona);

            Town town = session.find(Town.class, 2);
            Attraction attraction = session.find(Attraction.class, 4);
            town.getAttractions().add(attraction);
            attraction.getTowns().add(town);
            session.save(town);


            session.getTransaction().commit();
        }catch (Throwable e){
            System.out.println("ROLLBACK");
            e.printStackTrace();
        }
    }
}